from flask_table import Table, Col, LinkCol

class Results(Table):
    user_id = Col('EmpId', show=True)
    user_name = Col('EmpName')
    user_email = Col('Email')
    user_department = Col('Department', show=True)
    edit = LinkCol('Edit', 'edit_view', url_kwargs=dict(id='user_id'))
    delete = LinkCol('Delete', 'delete_user', url_kwargs=dict(id='user_id'))