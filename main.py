import pymysql
from application import app
from details import Results
from database_config import mysql
from flask import flash, render_template, request, redirect

@app.route('/new_user')
def add_user_view():
	return render_template('add.html')
		
@app.route('/add', methods=['POST'])
def add_user():
	conn = None
	cursor = None
	try:		
		EmpName = request.form['inputName']
		Emp_email = request.form['inputEmail']
		Department = request.form['inputDepartment']
		# validate the received values
		if EmpName and Emp_email and Department and request.method == 'POST':
			sql = "INSERT INTO Emp_details(user_name, user_email, user_department) VALUES(%s, %s, %s)"
			data = (EmpName, Emp_email, Department,)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, data)
			conn.commit()
			flash('User added successfully!')
			return redirect('/')
		else:
			return 'Error while adding user'
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()
		
@app.route('/')
def users():
	conn = None
	cursor = None
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT * FROM Emp_details")
		rows = cursor.fetchall()
		table = Results(rows)
		table.border = True
		return render_template('users.html', table=table)
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()

@app.route('/edit/<int:id>')
def edit_view(id):
	conn = None
	cursor = None
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT * FROM Emp_details WHERE user_id=%s", id)
		row = cursor.fetchone()
		if row:
			return render_template('edit.html', row=row)
		else:
			return 'Error loading #{id}'.format(id=id)
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()

@app.route('/update', methods=['POST'])
def update_user():
	conn = None
	cursor = None
	try:		
		EmpName = request.form['inputName']
		Emp_email = request.form['inputEmail']
		Department = request.form['inputDepartment']
		EmpId = request.form['id']
		# validate the received values
		if EmpName and Emp_email and Department and EmpId and request.method == 'POST':
			sql = "UPDATE Emp_details SET user_name=%s, user_email=%s, user_Department=%s WHERE user_id=%s"
			data = (EmpName, Emp_email, Department, EmpId,)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, data)
			conn.commit()
			flash('User updated successfully!')
			return redirect('/')
		else:
			return 'Error while updating user'
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()
		
@app.route('/delete/<int:id>')
def delete_user(id):
	conn = None
	cursor = None
	try:
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute("DELETE FROM Emp_details WHERE user_id=%s", (id,))
		conn.commit()
		flash('User deleted successfully!')
		return redirect('/')
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()
		
if __name__ == "__main__":
    app.run(debug = True,port = 5002)
